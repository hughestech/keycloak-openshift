#!/bin/bash

#Creating new project name: "keycloak"
oc new-project keycloak

#Uploading database template in the project
oc create -f https://gitlab.com/hughestech/keycloak-openshift/raw/master/postgresql.json

#Uploading keycloak template in the project
oc create -f https://gitlab.com/hughestech/keycloak-openshift/raw/master/keycloak.json

#Creating the database app using the template
oc new-app postgresql-for-keycloak

#Creating the keycloak app using the template
oc new-app keycloak-server	

#cd ../..
#rm -rf kc