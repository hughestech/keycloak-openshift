Based on blog post on https://medium.com/@sbose78/running-keycloak-on-openshift-3-8d195c0daaf6


# Instructions

Run ` ./keycloak.sh ` 

Or:

` oc create -f https://gitlab.com/hughestech/keycloak-openshift/blob/master/keycloak.json`

` oc create -f https://gitlab.com/hughestech/keycloak-openshift/blob/master/postgresql.json`
